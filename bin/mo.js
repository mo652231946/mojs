#!/usr/bin/env node
var Liftoff = require('liftoff');
var argv = require('minimist')(process.argv.slice(2));
var path = require('path');
const MoJs = require("../lib/mo.class");
var fileUtil = require('../lib/fileUtil');

var cli = new Liftoff({
    name: 'mo', // 命令名字
    processTitle: 'mo',
    moduleName: 'mo',
    configName: 'appconfig',
    // only js supported!
    extensions: {
        '.js': null
    }
});

cli.launch({
    cwd: argv.r || argv.root,
    configPath: argv.f || argv.file
}, function(env) {
    new MoJs(env,argv).execute();
});