import React, { Component } from 'react';

// 详情页底部按扭组
/**
 * @imgUrl 图片url地址
 * @score 积分数
 */
export default class MMPrivacy extends React.Component{

    render() {
        let score =this.props.score || 0;
        let sc = score>0? <span className="item-score">{score}金币查看</span>:null;
        let iconx = score<=0?<span className={"item-play"}></span>:<span className="item-lock"></span>
        return (
            <div className={"sw-item"}>
                <div className={"item-box"}>
                    {iconx}
                    <div className="item-mask"></div>
                    {sc}
                    <img src={this.props.imgUrl}/>
                </div>

            </div>
        );
    }

}