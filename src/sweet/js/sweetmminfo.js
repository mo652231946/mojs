import React, { Component } from 'react';

// 详情页面 用户基本信息组件
// <SweetMMInfo data={}/>
export default class SweetMMInfo extends React.Component{

    render() {

        let data = this.props.data;
        return (
            <div style={{color:"#FFF"}}>
            <div className={"sw-mm-info"}>
                <div className="mm-info-cell first-cell">
                    <div className="infos">
                        <label className="title">{data.nickName}</label>
                        <label>{data.age}岁</label>
                        <label>{data.location}</label>
                    </div>
                    <i className="info-small-text">{data.signature}</i>
                </div>
                <div className="mm-info-cell last-cell">
                    <i className="sw-icon-follow">关注</i>
                </div>
            </div>
                <div className="sw-mm-line"></div>
            </div>
        );
    }

}