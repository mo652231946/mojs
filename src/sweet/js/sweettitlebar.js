import React, { Component } from 'react';


// 详情页面 各模块标题组件
export default class SweetTitleBar extends React.Component{

    render() {
        return (
            <div className={"sw-box-title"}>
                <i></i><h4>{this.props.title}({this.props.nums})</h4>
            </div>
        );
    }

}