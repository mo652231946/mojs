const data = {
    "data": {
        "userId": "TvL5xbRbJOvzpUTdcGh",
        "nickName": "逍遥游(●—●)",
        "birthday": "1996-01-18",
        "mobile": "13537641763",
        "sex": 1,
        "status": 1,
        "headUrl": "http://file.tiandoulive.com/group1/M00/00/0F/CgAAC1vL9cGAZoVXAABYhUl9eHE124.png",
        "middleHeadUrl": "http://file.tiandoulive.com/group1/M00/00/0F/CgAAC1vL9cCARVglAAEz-HLxoz8850.png",
        "smallHeadUrl": "http://file.tiandoulive.com/group1/M00/00/0F/CgAAC1vL9cGAZoVXAABYhUl9eHE124.png",
        "location": "广东省-深圳市",
        "height": "179",
        "weight": "61",
        "age": 23,
        "signature": "一个人的世界，两个人的写照！",
        "roomId": 6,
        "callStatus": "leave",
        "feeling": "单身",
        "close": {
            "list": [{
                "user": {
                    "userId": "pXslavgiPlbOjTWWYMJ",
                    "nickName": "静静不只是静静了",
                    "sex": 2,
                    "smallHeadUrl": "http://file.tiandoulive.com/group1/M00/00/0F/CgAAC1vL9ZqAKnE4AAAiyAmTrXg676.jpg",
                    "age": 19,
                    "signature": "聊聊",
                    "goldCoins": 0,
                    "userSetting": {
                        "isHideCharge": 0
                    },
                    "isComplete": false
                },
                "number": 134960
            },
                {
                    "user": {
                        "userId": "wyKlsrZ1yHnjZQe4QFR",
                        "nickName": "Winter",
                        "sex": 2,
                        "smallHeadUrl": "http://file_test.tiandoulive.com/group1/M00/00/01/CgAAEVve03CAUZzfAACm0mM8RuA205.jpg",
                        "age": 29,
                        "signature": "木木木哦",
                        "goldCoins": 0,
                        "userSetting": {
                            "isHideCharge": 0
                        },
                        "isComplete": false
                    },
                    "number": 17920
                },
                {
                    "user": {
                        "userId": "MYZ6mvflFLLzusW3vcd",
                        "nickName": "Xxxxx",
                        "sex": 2,
                        "smallHeadUrl": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvVmcOAaanqAAAIxgoX1p4012.png",
                        "goldCoins": 0,
                        "userSetting": {
                            "isHideCharge": 0
                        },
                        "isComplete": false
                    },
                    "number": 10040
                },
                {
                    "user": {
                        "userId": "dGcvcbCkdR17WTLcgke",
                        "nickName": "夏末的春天美美美",
                        "sex": 2,
                        "smallHeadUrl": "http://file_test.tiandoulive.com/group1/M00/00/02/CgAAEVwMj3aAKI9dAAB03Qy7BrU979.png",
                        "age": 7,
                        "signature": "来来来",
                        "goldCoins": 0,
                        "userSetting": {
                            "isHideCharge": 0
                        },
                        "isComplete": false
                    },
                    "number": 5310
                },
                {
                    "user": {
                        "userId": "ivFs7KzKHbGOZfMOeT0",
                        "nickName": "过个个呃额滴",
                        "sex": 2,
                        "smallHeadUrl": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvQhnCAdFatAAD0qgRtk-Q116.png",
                        "age": 19,
                        "signature": "高规格尺寸的注意",
                        "goldCoins": 0,
                        "userSetting": {
                            "isHideCharge": 0
                        },
                        "isComplete": false
                    },
                    "number": 2320
                }
            ],
            "number": 9
        },
        "privateSpaceNumber": 0,
        "dynamicNumber": 0,
        "isFlow": 0,
        "giftNumber": 51,
        "goldCoins": 167082,
        "gifts": [
            {
                "id": 38,
                "number": 7,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPFxeAFGsNAACCXM81pMk142.png"
            },
            {
                "id": 40,
                "number": 1,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPF1CAY_9dAAB-ZzOcM7k541.png"
            },
            {
                "id": 44,
                "number": 2,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPF2OAZc5NAACsmzZOtk8630.png"
            },
            {
                "id": 39,
                "number": 2,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPF0uAJsN4AADeFjUAE3I198.png"
            },
            {
                "id": 43,
                "number": 1,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPF16AGBaUAADrZSdllvQ755.png"
            },
            {
                "id": 52,
                "number": 2,
                "giftIcon": "http://file_test.tiandoulive.com/group1/M00/00/00/CgAAEVvPF4aAbjC4AADLJ6sCd5I062.png"
            }
        ],
        "userSetting": {
            "isHideCharge": 0
        },
        "isComplete": false,
        "photoNumber": 0,
        "temlables": "高富帅,企业高管,颜值爆表",
        "userChatLables": "运动健康,电影,交友,旅行,美食,音乐,文学",
        "oppositeSexFeature": "声音甜,小鸟依人,温柔体贴,健身达人,大长腿,小蛮腰"
    },
    "code": "0000",
    "msg": ""
}

import HttpClient from "./httpclient";

const httpClient = new HttpClient("//"+location.host);

export default class SweetService {

    getParams(name){
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }

    // 根据sweet的id进行接口调用
    querySweetInfoById(sweetId){
        console.log("start send request................");
      return  httpClient.send("user/detail/info",{memberId:sweetId});
    }

}