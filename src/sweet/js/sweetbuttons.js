import React, { Component } from 'react';

// 详情页底部按扭组
export default class SweetButtons extends React.Component{

    render() {
        return (
            <div className={"sw-btns"}>
                <a className="btn1" href="javascript:;">礼物</a>
                <a className="btn2" href="javascript:;">私信</a>
                <a className="btn3 no-cur" href="javascript:;">相片</a>
            </div>
        );
    }

}