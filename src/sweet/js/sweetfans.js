import React, { Component } from 'react';

// 详情页面 六个图片带红色数字组件
// <SweetFans images={}/>
export default class SweetFans extends React.Component{



    render() {

        let self = this;
        let item =[0,1,2,3,4];
        let items = item.map(function (curindex,index,attr) {

            let img =  self.props.images[curindex] || null;

            let imgUrl = img.url;
            let num = img.num;

            return <div key={"k19-"+index} className="fans-item">
                {imgUrl?<img src={imgUrl}/>:null}
                {num && num>0?<i className="btn-radius">{num}分</i>:null}
            </div>;
        });
        return (
            <div className="sw-img-fans">
                <div className="fans-box">
                    {items}
                </div>

            </div>
        );
    }

}