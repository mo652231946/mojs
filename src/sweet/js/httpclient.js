const $ = require("zeptojs");
export default class HttpClient{


    constructor(host) {
        this.host = host;
    }

    getUrl(api){
        let urls = [];
        urls.push(this.host);
        urls.push(api);
        return urls.join("/");
    }

    send(url,query){
        let self = this;
        return new Promise(function (resolve,reject) {


            self.sign(url).then(function (res) {
                let data = {
                    "sign": res.data.server_sign,
                    "time_stamp": res.data.server_time_stamp,
                    "app_key": "tiandou_h5",
                    "app_version": "1.0",
                    "system_source": "h5",
            };
                $.post(self.getUrl(url),$.extend(data,query),function (dataSet) {
                    console.log("get info response result ",dataSet);
                    if(dataSet.code == "0000"){
                        resolve(dataSet.data);
                    }else{
                        resolve(null);
                    }

                });
            });
        });

    }



    sign(method) {
        let self = this;
        let mm = method.toString().replace(/\//gi,".");
        return new Promise((resolve, reject) => {
                $.ajax({
                    type: "POST",
                    cache: false,
                    url: self.getUrl("common/sign/get"),
                    data: {"ajax_method": mm, "app_key": "tiandou_h5"},
                    dataType:"json",
                    success: function (res) {
                        if (res.code = '0000') {
                            let db_sign = res.data.server_sign;
                            let time_stamp = res.data.server_time_stamp;
                            resolve(res);
                        } else {
                            alert("操作失败,稍后重试");
                            // console.log(res.msg);
                            reject(res);
                        }
                    },
                    error: function () {
                        alert("请求失败")//提示请求失败，稍后重试
                    }
                });
            }
        );
    }

}