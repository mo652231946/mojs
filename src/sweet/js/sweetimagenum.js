import React, { Component } from 'react';

// 详情页面 六个图片带红色数字组件
// <SweetImageNumber size={6} images={} imgCss={"img-small"}/>
export default class SweetImageNumber extends React.Component{



    render() {

        let self = this;
        let item =[];
        let $size = this.props.size || 6;
        console.log($size);
        for(let x=0;x<$size;x++){
            item.push(x);
        }

        let items = item.map(function (curindex,index,attr) {

           let img =  self.props.images[curindex] || null;

           if(null == img){
               return <div key={"k09-"+index} className="sw-img-item">
               </div>;
           }else{
               let imgUrl = img.url;
               let num = img.num;
               return <div key={"k09-"+index} className="sw-img-item">
                   {imgUrl?<img className={self.props.imgCss} src={imgUrl}/>:null}
                   {num && num>0?<i>{num}</i>:null}
               </div>;
           }

        });
        return (
            <div className="sw-img-number">
                <div className="img-wrapper">
                    {items}
                </div>

            </div>
        );
    }

}