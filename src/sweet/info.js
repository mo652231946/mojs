import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import {SweetSwiper} from "./sweet-swiper";
import Swiper from "swiper";

const $ = require("zeptojs");

require('./sweet.scss');
require("swiper/dist/css/swiper.css");
let pp = require("./img/pic1.png");
let pp1 = require("./img/mm001.png");
let pp2 = require("./img/mm002.png");
let pp3 = require("./img/mm003.png");
let pp4 = require("./img/mm004.png");


let gg1 = require("./img/gift-1.png");
let gg2 = require("./img/gift-2.png");
let gg3 = require("./img/gift-3.png");
let gg4 = require("./img/gift-4.png");



import SweetTitleBar from "./js/sweettitlebar";
import SweetMMInfo from  "./js/sweetmminfo";
import SweetButtons from  "./js/sweetbuttons";
import SweetImageNumber from  "./js/sweetimagenum";
import SweetFans from  "./js/sweetfans";
import MMPrivacy from  "./js/mmprivacy";

import SweetService from  "./js/sweetservice";

const sweetService = new SweetService();

// 轮播图片
class SweetSwiper extends React.Component{
    componentDidMount() {
        // console.log("0000000");
        var swiper = new Swiper('.swiper-container', {
            pagination: {
                el: '.swiper-pagination',
            },
        });
    }

    render() {
        return (
            <div className="sweet-swiper">
                <div className="swiper-container">
                    <div className="swiper-wrapper">
                        {this.props.children}
                    </div>
                    <div className="swiper-pagination"></div>
                </div>
            </div>
        );
    }
}
// 甜豆详情页面入口
class AppInfo extends React.Component{


    constructor(props, context) {
        super(props, context);
        this.state = {data:null};
    }

    componentDidMount() {
        let self = this;
        setTimeout(function () {
            // let sweetId = "TvL5xbRbJOvzpUTdcGh";
            let sweetId = sweetService.getParams("id");
            console.log("sweet member id ",sweetId);
            sweetService.querySweetInfoById(sweetId).then((result)=>{
                console.log("get member info ",result);
                self.setState({"data":result});
            });
        },10);
    }

    //动态
    showDynamic(attr,number){

        console.log("动态",attr);
        if(null == attr || attr.length<=0){
            return null;
        }

        return  <div className="sw-box">
            <SweetTitleBar title={"动态"} nums={number || 0}></SweetTitleBar>
            <div className={"sw-item-box"}>
                {
                    attr.map((url,index)=>{
                       return <div className={"sw-item"} key={"dynamics-"+index}><img src={url.coverURL}/></div>
                    })
                }
            </div>
        </div>
    }

    //私密空间
    showPrivateSpace(attr,number){

        if(null == attr || attr.length <=0){
            return null;
        }

        return <div className="sw-box">
            <SweetTitleBar title={"私密空间"} nums={number || 0}></SweetTitleBar>
            <div className={"sw-item-box"}>
                {
                    attr.map((url,index)=>{
                        return <MMPrivacy key={index+"-00"}  score={0} imgUrl={url.coverURL}/>
                    })
                }
            </div>
        </div>
    }


    //亲密朋友
    showClose(data){

        if(null == data || data["list"] == null){
            return null;
        }

        let list = data.list;
        let  patronSaintList = list.map(function (item,index) {

            let smallHeadUrl = null;
            if(null!=item.user && item.user.smallHeadUrl){
                smallHeadUrl = item.user.smallHeadUrl;
            }
            return {
                url:smallHeadUrl,
                num:item.number
            }
        });

        return   <div className="sw-box">
            <SweetTitleBar title={"亲密朋友"} nums={data.number}></SweetTitleBar>
            <SweetImageNumber images={patronSaintList}/>
        </div>
    }


    render() {

        let self = this;
        let image3 = [];
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});
        image3.push({url:"http://localhost:8080/sweet/assets/img/mm001.png",num:8});

        let img4 = [];
        img4.push({url:"http://localhost:8080/sweet/assets/img/gift-1.png",num:8});
        img4.push({url:"http://localhost:8080/sweet/assets/img/gift-2.png",num:8});
        img4.push({url:"http://localhost:8080/sweet/assets/img/gift-3.png",num:8});
        img4.push({url:"http://localhost:8080/sweet/assets/img/gift-4.png",num:8});
        let queryData =this.state.data;

        if(null == queryData){
            return <div></div>;
        }

        //守护神
        let patronSaintList=[];
        let patronSaintEl =null;
        if(queryData.patronSaint && queryData.patronSaint.length>0){
            patronSaintList = queryData.patronSaint.map(function (item,index) {
                return {
                    url:item.user.smallHeadUrl,
                    num:item.number
                }
            });

            patronSaintEl =   <div className="sw-box">
                <SweetTitleBar title={"她的守护神"} nums={queryData.patronSaint.number}></SweetTitleBar>
                <SweetImageNumber images={patronSaintList}/>
            </div>
        }

        //粉丝榜
        let fansList = [];
        let fansEl =  null;
        if(queryData.fans && queryData.fans.length>0){
            fansList = queryData.fans.map(function (item,index) {
                return {
                    url:item.user.smallHeadUrl,
                    num:item.number
                }
            });

            fansEl =   <div className="sw-box">
                <SweetTitleBar title={"粉丝榜"} nums={queryData.fans.number}></SweetTitleBar>
                <SweetFans images={fansList}/>
            </div>;
        }

        //收到的礼物
        let giftsList = [];
        let giftsEl = null;
        if(queryData.gifts && queryData.gifts.length>0){
            giftsList = queryData.gifts.map(function (item,index) {
                return {
                    url:item.giftIcon,
                    num:item.number
                }
            });

            giftsEl =    <div className="sw-box">
                <SweetTitleBar title={"收到礼物"} nums={queryData.giftNumber}></SweetTitleBar>
                <SweetImageNumber size={5} images={giftsList} imgCss={"img-small"}/>
            </div>;
        }

        // 动态



        return (
            <div>

                <div className={"sw-swiper-box"}>
                    <div className="swiper-box sw-icon-9"></div>
                    <div className="swiper-box sw-icon-10"></div>
                    <div className="sw-icon-100">{queryData.goldPermin || 0}&nbsp;金币/分</div>
                    <SweetSwiper>
                        <div className="swiper-slide"><img src={queryData.headUrl}/></div>

                    </SweetSwiper>
                </div>

                <SweetMMInfo data={queryData}></SweetMMInfo>


                <div className="sw-lables">
                    <ul>
                        {queryData.height? <li><label>身高：</label><span>{queryData.height}cm</span></li>:null}

                        {queryData.weight? <li><label>体重：</label><span>{queryData.weight}kg</span></li>:null}

                        {queryData.bust? <li><label>胸围：</label><span>{queryData.bust}</span></li>:null}
                        {queryData.satisfyPart?<li><label>最满意部位：</label><span>{queryData.satisfyPart}</span></li>:null}
                        {queryData.feeling? <li><label>感情状况：</label><span>{queryData.feeling}</span></li>:null}
                        {queryData.temlables?<li><label>标签：</label><span>{queryData.temlables}</span></li>:null}
                        {queryData.userChatLables?<li><label>兴趣爱好：</label><span>{queryData.userChatLables}</span></li>:null}
                        {queryData.datingMethod?<li><label>最喜欢的约会方式：</label><span>{queryData.datingMethod}</span></li>:null}
                        {queryData.oppositeSexFeature?<li><label>喜欢的异性特点：</label><span>{queryData.oppositeSexFeature}</span></li>:null}
                    </ul>
                </div>
                {self.showDynamic(queryData.dynamics,queryData.dynamicNumber )}
                {self.showPrivateSpace(queryData.privateSpaceUrl,queryData.privateSpaceNumber )}
                {self.showClose(queryData.close)}
                {patronSaintEl}
                {fansList}
                {giftsEl}
                <SweetButtons></SweetButtons>
            </div>
        );
    }
}

// 进行React渲染
ReactDOM.render(<AppInfo/>,document.getElementById("RootApp"),
    function () {
    $(document).click(function () {
        location.href = "http://www.tiandoulive.com";
    });

});