import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import {SweetSwiper} from "./sweet-swiper";

require('./sweet.scss');



// 甜豆详情页面入口
class AppConent extends React.Component{

    render() {



        return (
            <div className="ui-sw-cc ui-sw-o">
                <div className="ui-sw-c"></div>
                <div className="cc-b">
                    <p className="cc-f1">
                        本平台致力于为用户创造健康、和谐、积极向上的环境，严禁用户传播以下内容，否则一经查处，立即封号：
                    </p>
                    <div className="cc-bt">
                        <ul className="cc-list">
                            <li><i>01</i><p>严禁讨论和传播内容包含裸露、色情、暴力、赌博等违法内容</p></li>
                            <li><i>02</i><p>严禁在平台讨论政治等敏感话题、未授权的舆论等话题内容</p></li>
                            <li><i>03</i><p>严禁用户冒充平台或者其他工作人员对用户进行诱导</p></li>
                            <li><i>04</i><p>严禁用户将其他用户引导至其他第三方平台</p></li>
                            <li><i>05</i><p>严禁用户在平台内以任何形式发布广告</p></li>
                            <li><i>06</i><p>严禁使用期间出现冒犯、侮辱其他用户行为</p></li>
                            <li><i>07</i><p>严禁违反平台《用户协议和隐私条款》中规定内容</p></li>
                        </ul>
                    </div>

                    <p className="cc-f1">以上条例严格执行，如有用户违反，平台将对其作出永久封停处罚。</p>
                    <p className="cc-f1">最终解释权归平台所有，望用户能遵守平台规则，文明交友</p>
                </div>
            </div>
        );
    }
}

// 进行React渲染
ReactDOM.render(<AppConent/>,document.getElementById("RootApp"),
    function () {
    });