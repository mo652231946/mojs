import React, { Component } from 'react';
import ReactDOM from 'react-dom';


require('./css/mobile.scss');

class AppInfo extends React.Component{

    render() {


        return (
            <div>
                <div className={"ui-m"}>
                    <div className={"ui-mb"}><a href="hw.html" className={"mb-icon mb-hw"}></a></div>
                    <div className={"ui-mb"}><a href={"oppo.html"} className={"mb-icon mb-oppo"}></a></div>
                </div>

                <div className={"ui-m"}>
                    <div className={"ui-mb"}><a href="xiaomi.html" className={"mb-icon mb-xm1"}></a></div>
                    <div className={"ui-mb"}><a href="vivo.html" className={"mb-icon mb-vivo"}></a></div>
                </div>

                <div className={"ui-m"}>
                    <div className={"ui-mb"}><a href="mz.html" className={"mb-icon mb-xm"}></a></div>
                    <div className={"ui-mb ui-mb-not"}></div>
                </div>

            </div>
        );
    }
}

// 进行React渲染
ReactDOM.render(<AppInfo/>,document.getElementById("RootApp"),
    function () {
    });