/**
 *
 * @type {{}}
 */
const ExtractTextPlugin = require("extract-text-webpack-plugin");

var path = require("path");
var MyHtmlPlugin = require("./html-plugin");
// var presetsList = [];
// var __envKey = require.resolve("babel-preset-env");
// presetsList.push([__envKey,{"targets":{"browsers":["last 2 versions", "ie 8"]}}]);
// presetsList.push(require.resolve("babel-preset-react"));
module.exports  = function (env) {
    return {
        entry:{},
        plugins:[
            new MyHtmlPlugin({options: ''})
        ],
        stats:"errors-only",
        context:env.cwd,
        module:{
            rules:[

                {
                    test: /\.(js|jsx)$/,
                    use: {
                        loader: 'babel-loader'
                    },
                    exclude: [
                        path.resolve(__dirname, '../node_modules/'),
                        path.resolve(env.cwd, 'node_modules/')
                    ]
                },
                {
                    test: /\.css$/,
                    use:  ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    // If you are having trouble with urls not resolving add this setting.
                                    // See https://github.com/webpack-contrib/css-loader#url
                                    url: true,
                                    minimize: true,
                                    sourceMap: true,
                                    modules:false
                                }
                            }
                        ]
                    })
                },
                {
                    test: /\.scss$/,
                    use:  ExtractTextPlugin.extract(
                        {
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    // If you are having trouble with urls not resolving add this setting.
                                    // See https://github.com/webpack-contrib/css-loader#url
                                    url: true,
                                    minimize: true,
                                    sourceMap: true,
                                    modules:false
                                }
                            },
                            'sass-loader'
                        ]
                    })
                },
                // {test:/\.(png|svg|jpg|gif)$/, use: ['file-loader']},
                {test:/\.tsx?$/, use: "ts-loader",exclude: /node_modules/}

            ]

        },
        resolve:{
            extensions: [ '.tsx', '.ts', '.js',".css",'.jsx' ],
            modules:[
                "node_modules",
                path.resolve(__dirname, '../node_modules'),
            ]
        },
        resolveLoader:{
            modules:[
                "node_modules",
                path.resolve(__dirname, '../node_modules')

            ]
        },
        externals: {
            jquery: 'window.$'
        }
    }

}