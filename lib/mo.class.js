//noinspection JSUnresolvedFunction
/**
 *webpack 项目打包集成入口
 */
var path = require("path");
var fs = require("fs");

var HtmlWebpackPlugin = require('html-webpack-plugin');
var fileUtil = require('./fileUtil');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const open = require('opn');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpackBaseConfig = require("./webpack.config.js");
const webpackDevServer = require('webpack-dev-server');
const defaultConfig = require('./default.mo.js');  //项目默认配置
const extend = require('extend');


class MoJs{

    constructor(env,args){
        this.root = env.cwd;  // 目项根目录
        this.__env = env;  //Liftoff env 参数
        this.__args = args; // Liftoff args 参数
        this._webpack = require("webpack");
        this.__server = null;
        this._config = [];
        this.contentBase = [];
        // console.log(env,args);
        var configPath = path.resolve(env.cwd,"appconfig.js");
        var opts = this.__args["_"];
        this.options = {"start":opts[0],"opt":opts[1] || ""};
        if(fs.existsSync(configPath)){
            this.projectConfig = extend(true,defaultConfig,this.loadProjectConfigFile(configPath)); //加载项目配置文件
        }else{
            // console.error("not foud appconfig.js",configPath);
            this.projectConfig = extend(true,defaultConfig,{});
        }
        this.multiDirectory();
    }

    loadProjectConfigFile(configPath){
        var config = require(configPath);
        if(typeof config == "function"){
            return config();
        }
        return config;
    }


     /**
      * 根据文件名生成Entry 键名
      * @param modulePath
      * @param filename
      * @returns {*}
      */
    getEntryKey(modulePath,filename){
        var entryName = modulePath.replace(this.__env.cwd,"");
        entryName = entryName.replace(/^[\\\/]|[\\\/]$/gi,"");
        return filename;
    }

    clearRoot(pp){
        var entryName = pp.replace(this.__env.cwd,"");
        entryName = entryName.replace(/^[\\\/]|[\\\/]$/gi,"");
        return entryName;
    }
     /**
      * 全局通用js 与css 打包配置
      */
    commonConfig(){
         var self = this;
         var  __config = Object.assign({},webpackBaseConfig(self.__env));
         __config.entry = {
             'common':[
                 "es6-shim",
                 "es6-shim/es6-sham",
                 "@babel/polyfill",
                 "react",
                 "react-dom",
                 "react-router-dom",
                 "react-transition-group",path.resolve(__dirname, 'module/common.js')]
         };

         if(self.projectConfig.platform == "pc"){
             // __config.entry.common.push(path.resolve(__dirname, 'module/common.jquery.js'));
         }else {
             __config.entry.common.push(path.resolve(__dirname, 'module/common.zeptojs.js'));
         }
         __config.plugins.push( new CleanWebpackPlugin([path.resolve(self.__env.cwd,"../dist","assets")],
             {root:path.resolve(self.__env.cwd,"../")}));
         __config.plugins.push(new ExtractTextPlugin('css/[name].bundle.css'));

         __config.plugins.push(new this._webpack.optimize.CommonsChunkPlugin({
             names:['common'], // 注意不要.js后缀
             filename:'js/[name].bundle.js',
             minChunks:'Infinity'
         }));

         __config.externals = [];

         //这里可以修改相对路径
         __config["output"] = {
             path:path.join(self.__env.cwd,"../dist","assets"),
             filename:'js/[name].bundle.js',
             publicPath:'./assets/'
         }
         self.contentBase.push(path.join(self.__env.cwd,"../dist","assets"));
         self._config.push(Object.assign({},__config));
    }


    /**
     * 根据项目的约定目录结构生成webpack配置项-多目录多页面
     */
    multiDirectory(){
        var self = this;
        self.commonConfig();
        let mulitModule = {};
        if(this.__args["dir"] && (this.options.start == "start" || this.options.start == "release")){
            //针对指定目录编译
            mulitModule = fileUtil.forEachModule(this.__env.cwd,path.resolve(this.__env.cwd,this.__args["dir"]));
            // files = fileUtil.forEachFile(path.resolve(this.__env.cwd,this.__args["dir"]));
        }else{
            //编译整个项目
            mulitModule = fileUtil.forEachModule(this.__env.cwd,null);
            // files = fileUtil.forEachFile(this.__env.cwd);
        }

        // 遍历mulitModule 进行动态配置
        for(let moduleName in mulitModule){
            let files = mulitModule[moduleName];
            let __config = Object.assign({},webpackBaseConfig(self.__env));
            //配置输出目录
            self.contentBase.push(path.resolve(self.__env.cwd,"../dist",moduleName));
            __config["output"] = {
                publicPath:"./", //这里个改相对路径
                path:path.resolve(self.__env.cwd,"../dist",moduleName),
                filename:'[name]-[chunkhash].bundle.js'
            }


            // 对css 图片进行配置打包
            __config.module.rules.push({
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit:5000,
                            publicPath:"../img",
                            name:"[name].[ext]",
                            outputPath:"assets/img/"
                        }
                    }
                ]
            });

            //对多页面进行打包配置
            files.forEach(function (items) {
                var rp = items.modulePath.replace(self.__env.cwd,"");
                rp=rp.replace(/^[\\\/]|[\\\/]$/gi,"");

                // js 文件的输出路径
                var entryName = "assets/js/"+items.name;
                // var entryName = rp+"/assets/js/"+items.name;
                entryName = entryName.replace(/^[\\\/]|[\\\/]$/gi,"");
                console.log("entryName:%s,rp:%s",entryName,rp);
                var xx = {};
                xx[entryName] = items.jsPath;
                __config.entry = Object.assign(__config.entry,xx);

                // html文件的输出路径
                var outFileName = items.filename;
                outFileName=outFileName.replace(/^[\\\/]|[\\\/]$/gi,"");
                __config.plugins.push(new HtmlWebpackPlugin({
                    filename:outFileName,
                    inject:true,
                    template:self.clearRoot(items.html),
                    chunks:[entryName]
                }));






                __config.plugins.push(new ExtractTextPlugin({
                    filename:function (getPath) {
                        return getPath("[name].style-[contenthash].css").replace("/js/","/css/");
                    }
                }));
                __config.plugins.push( new CleanWebpackPlugin([path.resolve(self.__env.cwd,'../dist',rp)],{root:path.resolve(self.__env.cwd,'../')}));
            });

            self._config.push(Object.assign({},__config)); //装载配置
        }

    }


    listen(compiler,port){
        var self = this;
        if(this.__server == null){
            // console.log("rootWww:%s",path.join(self.__env.cwd,"../dist"));
            this.__server = new webpackDevServer(compiler,{
                contentBase:false,
                hot:true,
                progress:true,
                clientLogLevel:"none",
                stats:"errors-only"
            }).listen(port,self.projectConfig.server.host,function () {
                let dirs = self.__args["dir"] || '';
                var url = self.projectConfig.server.host+":"+self.projectConfig.server.port+"/"+dirs;
                open("http://"+url, {}).catch(() => {});
            });
        }
    }
    /**
     * 执行webpack 打包编译
     */
    execute(){
        var self = this;
        let configs = this._config;

        // console.log(JSON.stringify(configs,true,4));
        // configs.push({
        //     entry:"",
        //     output:{
        //         path:path.join(self.__env.cwd,"../dist")
        //     }
        // });
        let compiler = this._webpack(configs);

        compiler.plugin("done",function ()
            {
                        console.warn("己编译完成");
            }
        );
        //编译与打开页面 热更新
        if(self.options.start == "start"){
            self.listen(compiler,self.projectConfig.server.port);
        }

        // 只是编译
        if(self.options.start == "release"){
            compiler.run(function () {});
        }

    }

}
module.exports = MoJs;