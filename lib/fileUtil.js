/**
 * Created by Administrator on 2017/7/5.
 */
var fs = require("fs");
var path = require("path");


function fileutil() {
    this.__ig = ["_template_def","_template_react","xxx","modules",".idea","static","aaa","dist","css","assets","common"];
}

/**
 * 更新活动主目录
 */
fileutil.prototype.forEachDir = function (dirPath) {
    var self = this;
    var result = [];
    var files = fs.readdirSync(dirPath);
    files.forEach(function (filename) {
        // console.log(Array.contains(self.__ig,filename));

        if(self.__ig.indexOf(filename) == -1){
            var cPath = path.join(dirPath,filename);
            var info = fs.statSync(cPath);
            if(info.isDirectory()){
                var a =  self.forEachDir(cPath);
                result = result.concat(a);
            }
        }else{

        }
    });
    return result;
}

/**
 * 遍历目录和子目录下所有的.html,.js,.jsx文件
 * @param dirPath
 * @returns {Array}
 */
fileutil.prototype.forEachFile = function (dirPath) {
    var self = this;
    var result = [];
    var files = fs.readdirSync(dirPath);

    files.forEach(function (filename) {
        if(self.__ig.indexOf(filename) == -1){
            var cPath = path.join(dirPath,filename);
            var info = fs.statSync(cPath);

            if(info.isDirectory()){
                var a =  self.forEachFile(cPath);
                result = result.concat(a);
            }else{
                var ext = path.extname(cPath);

                var name = filename.replace(ext,"");
                var jsPath ="";
                var exts = [".js",".jsx",".ts"];
                for(var ixt in exts){
                    let pp = path.join(dirPath,name+exts[ixt]);
                    if(fs.existsSync(pp)){
                        jsPath = pp;
                        break;
                    }
                }

                //从js路径找
                if(jsPath.length<=0){
                    for(var ixt in exts){
                        let pp = path.join(dirPath,"js",name+exts[ixt]);
                        if(fs.existsSync(pp)){
                            jsPath = pp;
                            break;
                        }
                    }
                }

                if(filename.indexOf(".html") !=-1 && jsPath.length>0){
                    let pa = {
                        html:cPath,  //html 页面全路径
                        name:name,   // 页面的名称,不带扩展名
                        ext:exts,    // 可搜查的页面js文件的扩展名
                        modulePath:dirPath, // 当前页面所在的目录全路径
                        jsPath:jsPath, // 当前页面的js 全路径
                        filename:filename //页面的名称
                    };
                    result.push(pa);
                    // console.log("找到页面",pa);
                }

            }
        }else{

        }
    });
    return result;
}

// 解析出一级目录名称为模块名称
fileutil.prototype.getMoudelName = function(rootPath,mDir){
    // console.log("rootPath:%s,mDir:%s",rootPath,mDir);
    mDir = mDir.replace(rootPath,"");
    mDir = mDir.replace(/^\//,"");
    let i = mDir.indexOf("/");
    if(i==-1){
        return mDir;
    }
    return mDir.substring(0,i);
}
// 以dirPath 为根目录搜索该目录下的一级目录为子项目或模块名称,针对多目录打包的情况调用
fileutil.prototype.forEachModule = function (rootDir,subDir) {
    let rootPath = rootDir;
    if(null == subDir){
        subDir = rootPath;
    }
    let files = this.forEachFile(subDir);
    let modules = {};
    let self = this;
    // 将文件进行分组
    for(let i in files){
        let item = files[i];
        let mName = self.getMoudelName(rootPath,item.modulePath);
        mName= mName.toString().replace(/\\|\//gi,"");
        if(modules[mName]){
            modules[mName].push(item);
        }else{
            modules[mName] = [item];
        }
    }
    return modules;
}




module.exports = new fileutil();
// var ff = new fileutil();
// console.log(ff.forEachModule("/Users/lenli/Documents/workspace/mojs/src"));
// console.log(fs.existsSync("D:\\workspace\\activity\\src\\xx.xx"));