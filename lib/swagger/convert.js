var path = require("path");
var Swagger2Postman = require("swagger2-postman-generator");
Swagger2Postman
    .convertSwagger()
    .fromFile(path.resolve(__dirname,"./api-docs.json"))
    .toPostmanCollectionFile(path.resolve(__dirname,"./postman_collection.json"))


